var btnCalcular = document.getElementById("idBtn");
var selectForma = document.getElementById("idSelectForma");
var btnLimpar = document.getElementById("idBtnLimpar");

selectForma.addEventListener("change", function () {
  var raio = document.querySelector(".exibirRaio");
  var baseAltura = document.querySelector(".exibirBaseAltura");
  var lado = document.querySelector(".exibirLado");

  raio.style.display = "none";
  baseAltura.style.display = "none";
  lado.style.display = "none";

  if (selectForma.value == "circulo") {
    limpar()
    raio.style.display = "block";

  } else if (selectForma.value == "retangulo") {
    limpar()
    baseAltura.style.display = "block";
    
  } else if (selectForma.value == "triangulo retangulo") {
    limpar()
    baseAltura.style.display = "block";
    
  } else {
    limpar()
    lado.style.display = "block";
  }
});

btnCalcular.addEventListener("click", function () {
  var formaGeometrica = document.getElementById("idSelectForma").value;
  var raioCirculo = Number(document.getElementById("idRaioCirculo").value);
  var base = Number(document.getElementById("idBase").value);
  var altura = Number(document.getElementById("idAltura").value);
  var lado = Number(document.getElementById("idLado").value);
  document.getElementById("outResult").innerHTML = calcular(formaGeometrica, raioCirculo, base, altura, lado);
});

function calcular(formaGeometrica, raioCirculo, base, altura, lado) {
  if (formaGeometrica == "circulo") {
    let areaCirculo = 0;
    areaCirculo = Math.PI * (raioCirculo * 2);
    let msn = "Área do círculo é: " + areaCirculo.toFixed() + " cm²";
    return msn;

  } else if (formaGeometrica == "retangulo") {
    let areaRetangulo = 0;
    areaRetangulo = base * altura;
    let msn = "Área do retângulo é: " + areaRetangulo.toFixed() + " cm²";
    return msn;

  } else if (formaGeometrica == "triangulo retangulo") {
    let areaTriRet = 0;
    areaTriRet = (base * altura) / 2;
    let msn = "Área do triangulo retângulo é: " + areaTriRet.toFixed() + " cm²";
    return msn;

  } else {
    let areaTriEqui = 0;
    areaTriEqui = (Math.sqrt(3) * (lado * lado)) / 4;
    let msn = "Área do triangulo equilátero é: " + areaTriEqui.toFixed() + " cm²";
    return msn;
  }
}

btnLimpar.addEventListener("click", function () {
  limpar();
});

function limpar() {
  document.getElementById("idRaioCirculo").value = "";
  document.getElementById("idBase").value = "";
  document.getElementById("idAltura").value = "";
  document.getElementById("idLado").value = "";
  document.getElementById("outResult").innerHTML = "";
}